import os
import requests
import json
import traceback

from webbrowser import open_new_tab
from time import sleep

other_client_id = os.environ["OTHER_CLIENT_ID"]  # Other client id in the same project as IAP secured app
other_client_secret = os.environ["OTHER_CLIENT_SECRET"]  # Other client secret in the same project as IAP secured app (not a secret actually)
iap_client_id = os.environ["IAP_CLIENT_ID"]  # client id of the IAP secured app


# Script based on https://stackoverflow.com/a/49129038
# obtain ID token for provided Client ID: get authorization code -> exchange for refresh token -> obtain and return ID token
# More detailed description https://cloud.google.com/iap/docs/authentication-howto#authenticating_from_a_desktop_app
def id_token_from_client_id(client_id, client_secret, audience):
    auth_code = get_auth_code(client_id)
    refresh_token = get_refresh_token_from_code(auth_code, client_id, client_secret)
    print("REFRESH_TOKEN: {}".format(refresh_token))
    return id_token_from_refresh_token(client_id, client_secret, refresh_token, audience)




def get_auth_code(client_id):
    open_new_tab(
        "https://accounts.google.com/o/oauth2/v2/auth?client_id={}&response_type=code&scope=openid%20email&access_type=offline&redirect_uri=urn:ietf:wg:oauth:2.0:oob".format(
            client_id
        )
    )
    sleep(1)
    return input("Authorization code: ")


def get_refresh_token_from_code(auth_code, client_id, client_secret):
    oauth_token_base_URL = "https://www.googleapis.com/oauth2/v4/token"
    payload = {
        "code": auth_code,
        "client_id": client_id,
        "client_secret": client_secret,
        "redirect_uri": "urn:ietf:wg:oauth:2.0:oob",
        "grant_type": "authorization_code",
    }
    res = requests.post(oauth_token_base_URL, data=payload)
    print(json.loads(res.text))
    return str(json.loads(res.text)["refresh_token"])


def id_token_from_refresh_token(client_id, client_secret, refresh_token, audience):
    oauth_token_base_URL = "https://www.googleapis.com/oauth2/v4/token"
    payload = {
        "client_id": client_id,
        "client_secret": client_secret,
        "refresh_token": refresh_token,
        "grant_type": "refresh_token",
        "audience": audience,
    }
    res = requests.post(oauth_token_base_URL, data=payload)
    print(json.loads(res.text))
    return str(json.loads(res.text)["id_token"])


print(
    "ID_TOKEN: {}".format(id_token_from_client_id(other_client_id, other_client_secret, iap_client_id)
)  # other client ID should be from the same project as IAP Client ID

